<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = ['username', 'email', 'roles_id', 'name', 'id'];

    protected $keyType = 'string';

    public $incrementing = false;

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

}
